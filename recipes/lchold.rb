#
# Cookbook Name:: tp-vhost
# Recipe:: lchold
#
# Copyright 2013, Technophobia
#
# All rights reserved - Do Not Redistribute
#


directory "/home/lchold" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/lchold/sites/" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/lchold/sites/lenderscompared.org.uk" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/lchold/sites/lenderscompared.org.uk/certs" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end

directory "/home/lchold/sites/lenderscompared.org.uk/locking" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end

directory "/home/lchold/sites/lenderscompared.org.uk/htdocs" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/lchold/sites/lenderscompared.org.uk/logs" do
  owner "root"
  group "www-data"
  mode "0765"
  action :create
end
  
# Apache Config
template "/etc/apache2/sites-available/lenderscompared.org.uk" do
  source "lchold.erb"
  owner "root"
  group "root"
  mode "0644"
end
  
