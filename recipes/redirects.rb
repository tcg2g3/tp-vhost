#
# Cookbook Name:: tp-vhost
# Recipe:: substeps
#
# Copyright 2013, Technophobia
#
# All rights reserved - Do Not Redistribute
#

# Apache Config
template "/etc/apache2/sites-available/redirects.conf" do
  source "redirects.erb"
  owner "root"
  group "root"
  mode "0644"
end
  
link "/etc/apache2/sites-enabled/redirects.conf" do
  to "/etc/apache2/sites-available/redirects.conf" 
end
  
