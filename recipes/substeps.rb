#
# Cookbook Name:: tp-vhost
# Recipe:: substeps
#
# Copyright 2013, Technophobia
#
# All rights reserved - Do Not Redistribute
#

%w{ substeps code }.each do |site| 

  directory "/home/#{site}" do
    owner "root"
    group "root"
    mode "0755"
    action :create
  end
  
  directory "/home/#{site}/sites/" do
    owner "root"
    group "root"
    mode "0755"
    action :create
  end
  
  directory "/home/#{site}/sites/#{site}.technophobia.com" do
    owner "root"
    group "root"
    mode "0755"
    action :create
  end
  
  directory "/home/#{site}/sites/#{site}.technophobia.com/locking" do
    owner "root"
    group "root"
    mode "0755"
    action :create
  end

  directory "/home/#{site}/sites/#{site}.technophobia.com/htdocs" do
    owner "showcase"
    group "root"
    mode "0755"
    action :create
  end
  
  directory "/home/#{site}/sites/#{site}.technophobia.com/logs" do
    owner "root"
    group "www-data"
    mode "0765"
    action :create
  end
  
  # Apache Config
  template "/etc/apache2/sites-available/#{site}.technophobia.com" do
    source "#{site}.erb"
    owner "root"
    group "root"
    mode "0644"
  end
  
  link "/etc/apache2/sites-enabled/#{site}.technophobia.com" do
    to "/etc/apache2/sites-available/#{site}.technophobia.com" 
  end
  
end
