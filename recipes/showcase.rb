# Cookbook Name:: tp-vhost
# Recipe:: substeps
#
# Copyright 2013, Technophobia
#
# All rights reserved - Do Not Redistribute
#

sites = { "mlfrontline" => "GrufMelcen5", 
          "metrobank" => "d3POS1t", 
          "tsb" => "inheDruek7", 
          "dxgroup" => "deliveredexactly", 
          "tvl" => "hitisOsh5", 
          "lbbshowcase" => "!b4rnet", 
          "reed" => "IcnakGowf8", 
          "coopbank" => "9NatBoDriv", 
          "constructionline" => "kaddOdAg0",
          "sccmobile" => "RunOf1up",
	  "Capita_Travel_and_Events" => "RunifMer3",
	  "Barnet" => "ShokActEjis4",
	  "Barnet-MyAccount" => "patOijaj2",
	  "Lenders-Compared" => "SirHered2",
	  "Barnet-Directory" => "2OkModCyfs",
	  "Akinika" => "griadMong3",
	  "create-tomorrow" => "greacBil8",
	  "prudential" => "2QuiGiWryb",
          "tpr" => "vooshCop8",
	  "G2G3D-Website" => "G2G3D",
          "stratcomm" => "damVomUvnav5",
	  "clg" => "VeocWith4",
	  "iMitchell" => "Gadsyait5",
	  "capita-nccw" => "efyesDud3",
          "constructionline" => "WuckLyeg7",
	  "pcsux" => "dic9Dreij",
          "southamptoncc" => "Vetjirec4",
          "barnet-vcs" => "Kruabeev0",
          "pcs" => "KaggEacEuf8",
          "pip" => "f7tH3WE1L8x", 
          "omiga" => "PW40m1g4" }
          
user "showcase" do
  supports :manage_home => true
  comment "Showcase User"
  home "/home/showcase"
  shell "/bin/bash"
  action :create
end

directory "/home/showcase" do
  owner "showcase"
  group "root"
  mode "0755"
  action :create
end

directory "/home/showcase/sites/" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/showcase/sites/showcase.orangebus.co.uk" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
directory "/home/showcase/sites/showcase.orangebus.co.uk/locking" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end

directory "/home/showcase/sites/showcase.orangebus.co.uk/logs" do
  owner "root"
  group "www-data"
  mode "0765"
  action :create
end

directory "/home/showcase/sites/showcase.orangebus.co.uk/htdocs" do
  owner "root"
  group "root"
  mode "0755"
  action :create
end
  
sites.each do |site,password| 

  directory "/home/showcase/sites/showcase.orangebus.co.uk/htdocs/#{site}" do
    owner "showcase"
    group "root"
    mode "0755"
    action :create
  end
  
  bash "Set up htpasswords" do
    user "root"
    code <<-EOH
    /usr/bin/htpasswd -s -b -c /home/showcase/sites/showcase.orangebus.co.uk/locking/htpasswd.#{site} #{site} #{password}
    EOH
  end
end

#Metro Subdomain 
directory "/home/showcase/sites/showcase.orangebus.co.uk/htdocs/metrobank-subdomains" do
  owner "showcase"
  group "root"
  mode "0755"
  action :create
end

#package "libapache2-mod-php5" do 
#  action :install
#end

package "fail2ban" do 
  action :install
end

execute "a2dismod compatibility module" do
	command '/usr/sbin/a2dismod access_compat -f'
		only_if do ::File.exists?('/etc/apache2/mods-enabled/access_compat.load')end
end

#Create HealthCheck Files
directory "/home/showcase/showcase.orangebus.co.uk/htdocs/health" do
  owner "showcase"
  group "root"
  mode "0755"
  action :create
end

template "/home/showcase/showcase.orangebus.co.uk/htdocs/health/imalive.html" do
  source "imalive.html.erb"
  owner "showcase"
  group "root"
  mode "0644"
  variables :hostname => node[:ec2][:local_hostname]
end


# Apache Config
template "/etc/apache2/sites-available/showcase.orangebus.co.uk.conf" do
  source "showcase.erb"
  owner "root"
  group "root"
  mode "0644"
  variables :sites => sites
end
 
template "/etc/apache2/sites-available/health-showcase.orangebus.co.uk.conf" do
  source "health-showcase.erb"
  owner "root"
  group "root"
  mode "0644"
  variables :hostname => node[:ec2][:local_hostname]
end

link "/etc/apache2/sites-enabled/showcase.orangebus.co.uk.conf" do
  to "/etc/apache2/sites-available/showcase.orangebus.co.uk.conf" 
end

link "/etc/apache2/sites-enabled/health-showcase.orangebus.co.uk.conf" do
  to "/etc/apache2/sites-available/health-showcase.orangebus.co.uk.conf" 
end